<?php
/**
 * @package ContextualCode\AnalyzeErrorLogs
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 **/

namespace ContextualCode\AnalyzeErrorLogs;

require_once(__DIR__ . '/BaseAnalyzer.php');

class ApacheAnalyzer extends BaseAnalyzer {
    private static $suffix = '_error.log';
    private static $dir    = '/var/log/apache2';

    protected $logDateFormat = 'D M d H:i:s.u Y';
    protected $regExps = array(
        // Remove PID
        '/ \[pid (\d+)\]/i' => '',
        // Remove Client IP
        '/ \[client (\d+)\.(\d+)\.(\d+)\.(\d+):(\d+)\]/i' => '',
        // Ignoe LetsEncrypt errors
        '/(.*)\/\.well-known(.*)/i' => '',
        // https://serverfault.com/questions/708438/what-is-this-error-in-apache-error-log
        '/(.*)Got bogus version(.*)/i' => '',
        '/(.*)Error dispatching request to \: \(passing brigade to output filters\)(.*)/i' => '',
        // http://issuetrack.contextualcode.com/view.php?id=24461
        '/(.*)Call to a member function getTimestamp\(\) on null in \/srv\/www\/(\w+)\/vendor\/ezsystems\/ezpublish-kernel\/eZ\/Bundle\/EzPublishIOBundle\/BinaryStreamResponse\.php(.*)/i' => '',
        // https://bz.apache.org/bugzilla/show_bug.cgi?id=57198
        '/(.*)AH01070: Error parsing script headers(.*)/i' => '',
        // Strange requests made by bots
        '/(.*)AH01797: client denied by server configuration(.*)/i' => '',
        // This error happens when web node is hit directly omitting LB
        '/(.*)AH01092: no HTTP 0\.9 request(.*)/i' => '',
        // Timeout is monitored by StatusCake, so we can ignore this messages
        '/(.*)Connection timed out: AH00957(.*)/i' => '',
        '/(.*)AH01079: failed to make connection to backend: php(.*)/i' => '',
        // This error happens when web node is accesses trough 443 port directly
        '/(.*)AH02042: rejecting client initiated renegotiation(.*)/i' => '',
        // http://issuetrack.contextualcode.com/view.php?id=24515
        '/(.*)\(70008\)Partial results are valid but processing is incomplete: AH01075: Error dispatching request to : \(reading input brigade\)(.*)/i' => '',
        // AH01139: TRACE forbidden by server configuration
        '/(.*)TRACE forbidden by server configuration(.*)/' => '',
        // http://issuetrack.contextualcode.com/view.php?id=24536
        '/(.*) Call to a member function userIsParticipant\(\) on null in (.*)ezpublish_legacy\/kernel\/collaboration\/item\.php on line 20(.*)/' => ''
    );

    public function run() {
        $resultsBaseDir = dirname($this->files['results']) . '/';

        $logFiles = scandir(self::$dir);
        foreach($logFiles as $logFile) {
            if(strrpos($logFile,self::$suffix) === false) {
                continue;
            }

            $this->files['local'] = self::$dir . '/' . $logFile;

            $vhost = str_replace(self::$suffix,'',basename($logFile));
            $vhost = trim($vhost, '-_');
            if(empty($vhost)) {
                continue;
            }
            $this->setResultsFile($resultsBaseDir . 'apache2_' . $vhost . '.log');

            parent::run();
        }
    }
}
