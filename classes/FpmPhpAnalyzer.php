<?php
/**
 * @package ContextualCode\AnalyzeErrorLogs
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 **/

namespace ContextualCode\AnalyzeErrorLogs;

require_once(__DIR__ . '/BaseAnalyzer.php');

class FpmPhpAnalyzer extends BaseAnalyzer {
    protected $logDateFormat = 'd-M-Y H:i:s';
    protected $regExps = array(
        // Remove child PID
        '/child (\d+) said/i' => 'child XXX said',
        // Gitlab feeds
        '/https:\/\/gitlab\.com\/(\w+)\.atom/' => 'https://gitlab.com/XXX.atom',
        // Ignore notices
        '/^NOTICE: (.+)$/' => '',
        // http://issuetrack.contextualcode.com/view.php?id=24461
        '/(.*)Call to a member function getTimestamp\(\) on null in \/srv\/www\/(\w+)\/vendor\/ezsystems\/ezpublish-kernel\/eZ\/Bundle\/EzPublishIOBundle\/BinaryStreamResponse\.php(.*)/i' => '',
        // Machform mysql_connect
        '/(.*)use mysqli or PDO instead in \/srv\/www\/alabamacu\/src\/ThinkCreative\/MachformBundle\/Lib\/machform\/includes\/db-core\.php on line 18(.*)/i' => ''
    );

    public function __construct() {
        parent::__construct();

        $this->files['local'] = '/var/log/php5-fpm.log';
    }
}
