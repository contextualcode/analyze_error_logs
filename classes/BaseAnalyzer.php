<?php
/**
 * @package ContextualCode\AnalyzeErrorLogs
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 **/

namespace ContextualCode\AnalyzeErrorLogs;

use DateTime;
use DateInterval;

abstract class BaseAnalyzer {
    protected $outputBuffer = null;
    protected $files = array(
        'local'   => null,
        'results' => null
    );
    protected $logDateFormat = 'd-M-Y H:i:s T';
    protected $handleDateInterval = 'P7D';
    protected $regExps = array();

    public function __construct() {
    }

    public function setResultsFile($file) {
        $this->files['results'] = $file;
        exec('rm -f ' . $file);
    }

    public function setHandleDateInterval($handleDateInterval) {
        $this->handleDateInterval = $handleDateInterval;
    }

    public function run() {
        $today   = new DateTime('now');
        $minDate = $today->sub(new DateInterval($this->handleDateInterval));

        $errors = array();
        $lines  = $this->getLogLines();
        $this->output('Analyzing "' . $this->files['local'] . '" ...');
        foreach($lines as $line) {
            $lineData = $this->extractDataFromLogLine($line);
            if($lineData === null || isset($lineData['date']) === false) {
                continue;
            }

            $lineData['error'] = $this->fixLogLineError($lineData);
            if(empty($lineData['error'])) {
                continue;
            }

            $lineData['id'] = $this->getLogLineId($lineData);

            $date = DateTime::createFromFormat($this->logDateFormat, $lineData['date']);
            if($date === false || $date < $minDate) {
                continue;
            }

            if(isset($errors[$lineData['id']]) === false) {
                $errors[$lineData['id']] = array(
                    'text'  => $lineData['error'],
                    'dates' => array($lineData['date'])
                );
            } else {
                $errors[$lineData['id']]['dates'][] = $lineData['date'];
            }
        }

        if(count($errors) === 0) {
            $this->output('No errors found');
        } else {
            $this->storeResults($errors);
        }
    }

    public function getOutputBuffer() {
        return $this->outputBuffer;
    }

    protected function getLogLines() {
        if(file_exists($this->files['local']) === false) {
            $this->error('Local logs file "' . $this->files['local'] . '" is not available');
        }

        return file($this->files['local']);
    }

    protected function extractDataFromLogLine($line) {
        $separator = '] ';
        $tmp       = explode($separator, $line);
        if(count($tmp) < 2) {
            return null;
        }

        $date    = trim($tmp[0],'[] ');
        $error   = trim(implode($separator, array_slice($tmp,1)));

        return array(
            'date'  => $date,
            'error' => $error
        );
    }

    protected function fixLogLineError($lineData) {
        $error = $lineData['error'];
        foreach($this->regExps as $pattern => $replacement) {
            $error = preg_replace($pattern, $replacement, $error);
        }

        return trim($error);
    }

    protected function getLogLineId($lineData) {
        return md5($lineData['error']);
    }

    protected function storeResults($errors) {
        uasort($errors, function($a, $b) {
            return count($a['dates']) - count($b['dates']);
        });

        $fp = fopen($this->files['results'], 'w');
        if($fp === false) {
            $this->error('Can not open ' . $this->files['results'] . ' for write');
        }
        foreach($errors as $id => $error) {
            fwrite($fp, $id . ' [' . count($error['dates']) . ' times, ' . $error['dates'][count($error['dates']) - 1] . ']' . "\n" . $error['text'] . "\n");
        }
        $this->output(count($errors) . ' errors found and saved to ' . $this->files['results']);
    }

    protected function error($string) {
        $this->output('[ERROR] ' . $string);
        exit();
    }

    protected function output($string) {
        $string = '[' . date('c') . '] ' . $string . "\n";
        $this->outputBuffer .= $string;
        echo($string);
    }
}
