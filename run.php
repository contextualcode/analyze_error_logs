#!/usr/bin/env php
<?php
/**
 * @package ContextualCode\AnalyzeErrorLogs
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 **/

require_once('./classes/Helper.php');
use ContextualCode\AnalyzeErrorLogs\Helper;

$dir = './results';
if(file_exists($dir) === false) {
    mkdir($dir, 0755);
}
if(is_writable($dir) === false){
    exit('[ERROR] "' . $dir . '" is not writable' . "\n");
}

$map = array(
//    'php5-fpm'       => 'FpmPhpAnalyzer',
    'docker-fpm-php' => 'DockerFpmPhpAnalyzer',
    'apache2'        => 'ApacheAnalyzer',
    'nginx'          => 'NginxAnalyzer'
);
foreach($map as $class) {
    require_once('./classes/' . $class . '.php');
}

$options = getopt('',array('email_notification','analyzer::','date_interval::'));

$classes = array();
if(isset($options['analyzer'])) {
    if(isset($map[$options['analyzer']]) === false) {
        exit('[ERROR] Analyzer identifier is missing or invalid. Valid values are: ' . implode(', ', array_keys($map)) . "\n");
    }
    $classes = array($options['analyzer'] => $map[$options['analyzer']]);
} else {
    $classes = $map;
}

$output = null;
foreach($classes as $type => $class) {
    $class = 'ContextualCode\\AnalyzeErrorLogs\\' . $class;
    if(class_exists($class) === false) {
        echo('[ERROR] "' . $class . '" class is missing' . "\n");
        continue;
    }

    $output .= $type . "\n";

    $analyzer = new $class;
    if(isset($options['date_interval'])) {
        $analyzer->setHandleDateInterval($options['date_interval']);
    }
    $analyzer->setResultsFile(realpath('./results/') . '/' . $type . '.log');
    $analyzer->run();

    $output .= $analyzer->getOutputBuffer();
}

if(isset($options['email_notification'])) {
    Helper::sendEmail(array('serhey@contextualcode.com','chris@contextualcode.com','david@contextualcode.com'),$dir,'Errors Analyze Repost',$output);
}
